import werkzeug
from gevent.pywsgi import WSGIServer
from AUTO.sms_email_automate import *
from connection import *
from functions import *
from flask import Flask, jsonify
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)


@app.route('/sms-email-service', methods=['POST'])
@cross_origin()
def lambda_handler():
    warnings = []
    errors = {}
    headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, x-api-secret, x-api-key, access-control-allow-credentials',
    }

    cursor, conn, err = db_connect()
    if err is not None:
        errors["databaseError"] = err
        return {
        "statusCode": 502,
        "headers": headers,
        "body": json.loads(json.dumps({"status": False, "errors": errors}))
    }
    sftp, err1 = sftp_connection()
    if err1 is not None:
        errors["sftpError"] = err1
        return{
            "statusCode": 502,
            "headers": headers,
            "body": json.loads(json.dumps({"status": False, "errors": errors}))
        }

    name_list = file_name_filter(sftp)
    if name_list == []:
        # sftp.close()
        # logger.info('sftp_closed with No file found')
        resp_data = json.dumps({
            "statusCode": 200,
            "headers": headers,
            "body": json.loads(json.dumps({"status": False, "error": "No file found!"}))
        })
        return json.loads(resp_data)

    errors["documentError"] = []
    d = {}

    for filename in name_list:
        dataframe, error = read_from_sftp(filename, sftp, "/sftpuatdata/Morat-RPS/Request-IN")
        if error is not None:
            d["sftpReadError"] = error

        dataframe_resp, warnings, error = get_automate(dataframe, cursor, conn)

        for i in error:
            if i == "Bitly Error":
                return jsonify({
        "statusCode": 200,
        "headers": headers,
        "body": json.loads((json.dumps({"status": False, "warnings": warnings, "errors": error})))
    })

        if len(error) != 0:
            d["servicesError"] = error

        error = write_to_sftp_as_csv(filename, dataframe_resp, sftp, "/sftpuatdata/Morat-RPS/Response-Out")
        if error is not None:
            d["sftpWriteError"] = error

        error = move_to_archive(filename, "/sftpuatdata/Morat-RPS/Request-IN", sftp, "/sftpuatdata/Morat-RPS/Request-Archive")
        if error is not None:
            d["sftpArchivedError"] = error

        if len(d) != 0:
            errors["documentError"].append(d)

    # sftp.close()
    # logger.info('sftp_closed at last')

    if len(errors["documentError"]) == 0:
        del errors["documentError"]

    if len(errors) == 0:
        errors = None

    return jsonify({
        "statusCode": 200,
        "headers": headers,
        "body": json.loads((json.dumps({"status": True, "warnings": warnings, "errors": errors})))
    })

@werkzeug.serving.run_with_reloader
def main():
    http_server = WSGIServer(("0.0.0.0", int("5000")), app)
    http_server.serve_forever()
    return app


if __name__ == '__main__':
    app = main()

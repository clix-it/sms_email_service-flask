import requests
import json
import pandas as pd
import numpy as np
from json import loads
import json
import pysftp
import os
import logging
import datetime
import time_uuid
import random

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def file_name_filter(sftp):
    sftp.cwd("/sftpuatdata/Morat-RPS/Request-IN")
    filename1 = list()
    for i in sftp.listdir_attr():
        i = i.filename
        if i.endswith('xlsx') or i.endswith('xls'):
            filename1.append(i)
    logging.info(filename1)
    return filename1


def connect_to_sftp(myHostname, myUsername, myPassword):
    try:
        cnopts = pysftp.CnOpts(knownhosts='temp.pub')
        # cnopts.hostkeys = None
        sftp = pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts)
        return sftp, None
    except:
        try:
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None
            sftp = pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts)
            return sftp, None
        except Exception as e:
            logger.error(e)
            logger.info("SFTP connection failed!")
            return None, "SFTP connection failed!"


def read_from_sftp(filename, sftp, path):
    try:
        with sftp.open(f'{path}/{filename}', 'r+', bufsize=32768) as f:
            dataframe = pd.read_excel(f)
        logger.info('successfully read from sftp')
        return dataframe, None
    except Exception as e:
        logger.error(e)
        return pd.DataFrame(), "unable to read from sftp"


def column_check(dataframe):
    try:
        if 'EMAIL_ID' in dataframe and 'MOBILE_NO' in dataframe and 'LMS Name' in dataframe and 'LAN_NO' in dataframe:
                return dataframe, None
        else:
            return pd.DataFrame(), "Key error for column names"
    except Exception as e:
        logger.error(e)
        return pd.DataFrame(), "Empty Dataframe"


def write_to_sftp_as_csv(filename, dataframe, sftp, path, ):
    try:
        filename = str(filename).split('.')
        with sftp.open(f'{path}/{filename[0]}-{datetime.datetime.now()}.csv', 'w+', bufsize=32768) as f:
            f.write(dataframe.to_csv(index=False))
        logger.info('successfully_converted to csv')
        return None
    except Exception as e:
        logger.error(e)
        logger.info('write error')
        return "CSV write error in sftp"


def move_to_archive(filename, path_in, sftp, path_archive):
    try:
        sftp.rename(f'{path_in}/{filename}',
                    f'{path_archive}/{filename}')
        logger.info('archived successfully')
        return None
    except Exception as e:
        sftp.cwd(path_archive)
        sftp.remove(filename)
        msg = "new file with same name is added by deleting the previous one in archive directory"
        print(msg)
        logger.info(msg)
        try:
            sftp.rename(f'{path_in}/{filename}',
                        f'{path_archive}/{filename}')
            logger.info('archived successfully')
            return None
        except Exception as e:
            print(e)
            logger.error(e)
            return "sftp archived failed"


def get_time_uuid():
    rand_time = lambda: float(random.randrange(0, 30)) + time_uuid.utctime()
    return str(time_uuid.TimeUUID.with_timestamp(rand_time()))


# Define Exception class for retry
class RetryException(Exception):
    u_str = "Exception ({}) raised after {} tries."

    def __init__(self, exp, max_retry):
        self.exp = exp
        self.max_retry = max_retry

    def __unicode__(self):
        return self.u_str.format(self.exp, self.max_retry)

    def __str__(self):
        return self.__unicode__()

# Define retry util function
def retry_func(func, max_retry=10):
    """
    @param func: The function that needs to be retry
    @param max_retry: Maximum retry of `func` function, default is `10`
    @return: func
    @raise: RetryException if retries exceeded than max_retry
    """
    for retry in range(1, max_retry + 1):
        try:
            return func()
        except Exception as e:
            logger.info('Failed to call {}, in retry({}/{})'.format(func.func, retry, max_retry))
    else:
        raise RetryException(e, max_retry)


def bitly_link(bitly_address):
    error = None
    try:
        print("Calling bitly service...")
        r = requests.post(bitly_address,
                          data=json.dumps({"inurl": "https://my.clix.capital/login"}),
                          headers={'Content-type': 'application/json'})
        resp = r.json()['outurl']
        logger.info("bitly successful")
        bitly_status = True
    except Exception as e:
        resp = None
        logger.error(e)
        bitly_status = False
        error = "Bitly Error"
    return resp, bitly_status, error


def sms_automation(sms_service_address, req_id, resp, mobile_number):
    error = None
    try:
        sms_automate = requests.post(sms_service_address, data=json.dumps({
            "applicationId": req_id,
            "notificationType": "SMS",
            "contentCode": "REVISED_RPS_SMS",
            "personalizatioData": {
                "url": str(resp)
            },
            "to": [
                {
                    "mobile": str(mobile_number)
                }
            ]
        }), headers={'Content-type': 'application/json'})
        mob_resp = (sms_automate.json())
        mobile_status = True
        return mob_resp, mobile_status, None
    except Exception as e:
        logger.error(e)
        mobile_status = False
        mob_resp = {}
        error = 'sms_error'
        return mob_resp, mobile_status, error


def email_automate(email_service_address, req_id, resp, email):
    error = None
    try:
        email_automate = requests.post(email_service_address, data=json.dumps({
            "applicationId": req_id,
            "notificationType": "TRANS",
            "contentCode": "REVISED_RPS_EMAIL",
            "personalizationData": {
                "url": str(resp)
            },
            "to": [
                {
                    "emailid": str(email)
                }
            ],
            "source": "LAAS-EMAIL"
        }), headers={'Content-type': 'application/json'})
        email_resp = (email_automate.json())
        email_status = True
    except Exception as e:
        logger.error(e)
        email_resp = {}
        email_status = False
        error = "Email Error"
    return email_resp, email_status, error

import pandas as pd
import numpy as np
import pkg_resources
import requests
import json
#import psycopg2
import os
import re
import csv
import logging
from functions import *
import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_automate(data, cursor, conn):
    regex = '^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$'

    li = []
    errors = set()
    warnings = set()


    try:
        logger.info('connecting bitly...........')
        r = requests.post("https://app.c1ix.me", data=json.dumps({
                     "url": "https://my.clix.capital/login",
                      "expiry_days": 30}),    headers = {
                                     'Content-type': 'application/json',
                                         'x-api-key': 'QTYcM05hgV3eQBbANgoKN8vyND4j5dtl4jToSAsG'})
        resp = f"https://c1ix.me/{r.json()['short_url']}"
        logger.info('bitly successfull')
        bitly_status = True
    except Exception as e:
        logger.error(e)
        bitly_status = False
        resp = None

    for idx in range(len(data)):
        req_id = get_time_uuid()

        email = str(data['EMAIL_ID'][idx]).strip()

        validate_email = True if (re.search(regex, email)) else False

        mobile_number = int(data['MOBILE_NO'][idx]) if pd.notna(data['MOBILE_NO'][idx]) else 'none'

        Pattern = re.compile("(0/91)?[7-9][0-9]{9}")

        validate_mobile = True if Pattern.match(str(mobile_number)) else False
        lms_name = str(data['LMS Name'][idx])
        lan_number = str(data['LAN_NO'][idx])
        validate_lan = True if pd.notna(data['LAN_NO'][idx]) and lan_number.strip().lower() not in ['', 'none',
                                                                                                        'nan'] else False
        msg_list = []
        mobile_status = False
        mob_resp = None
        email_status = False
        email_resp = None
        if validate_lan:
            if bitly_status is True:
                if validate_mobile:
                    try:
                        sms_automate = requests.post("https://ms-uat.clix.capital/notification/v1/sms", data=json.dumps({
                            "applicationId": req_id,
                            "notificationType": "SMS",
                            "contentCode": "REVISED_RPS_SMS",
                            "personalizationData": {
                                "url": str(resp)
                            },
                            "to": [
                                {
                                    "mobile": str(mobile_number)
                                }
                            ]
                        }), headers={'Content-type': 'application/json'})
                        mob_resp = (sms_automate.json())
                        mobile_status = True

                    except Exception as e:
                        logger.error(e)
                        mobile_status = False
                        msg_list.append('sms_error:{}'.format(str(e)))
                        mob_resp = {}
                        errors.add('SMS Error')
                else:
                    mobile_status = False
                    msg_list.append('mobile_number_not_present/invalid_number')
                    warnings.add('mobile_number_not_present/invalid_number in some records')
                if validate_email:
                    try:
                        email_automate = requests.post("https://ms-uat.clix.capital/notification/v1/email", data=json.dumps({
                            "applicationId": req_id,
                            "notificationType": "TRANS",
                            "contentCode": "REVISED_RPS_EMAIL",
                            "personalizationData": {
                                "url": str(resp)
                            },
                            "to": [
                                {
                                    "emailid": str(email)
                                }
                            ],
                            "source": "LAAS-EMAIL"
                        }), headers={'Content-type': 'application/json'})
                        email_resp = (email_automate.json())
                        email_status = True
                    except Exception as e:
                        logger.error(e)
                        email_resp = {}
                        email_status = False
                        msg_list.append('Email error:{}'.format(str(e)))
                        errors.add('Email Error')
                else:
                    email_status = False
                    msg_list.append("Email not present/Invalid Email")
                    warnings.add("Email not present / invalid email in some records")
            else:
                bitly_status = False
                msg_list.append('Bitly status is false')
                errors.add('Bitly Error')
        else:
            msg_list.append('lan number not present')
            warnings.add('lan number not present in some records')
        try:
            li.append((mobile_number, mobile_status, mob_resp, email, email_status, email_resp, bitly_status, msg_list,
                       lan_number, lms_name, req_id))

            postgres_insert_query = "insert into  morat_sms_email_logs(mobile_number,mobile_status,mobile_resp,email_id," \
                                    "email_status," "email_resp,bitly_status,error_msgs,lan_no,lms_name, request_id) " \
                                    "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            record_to_insert = (mobile_number, mobile_status, json.dumps(mob_resp), email, email_status,
                                json.dumps(email_resp), bitly_status, msg_list, lan_number, lms_name, str(req_id))
            cursor.execute(postgres_insert_query, record_to_insert)
            conn.commit()
        except Exception as e:
            logger.error(e)
            if conn:
                logger.info("failed_to_insert_record in table", e)
                errors.add("failed_to_insert some record in table ")
            else:
                logger.info("database connection break!", )
                errors.add("database connection break")

    try:
        df1 = pd.DataFrame(li, columns=['mobile_number', 'mobile_status', 'mobile_resp', 'email_id', 'email_status',
                                        'email_resp',
                                        'bitly_status', 'error_msgs', 'lan_no', 'lms_name', 'request_id'])
        logger.info('record converted to dataframe')
    except Exception as e:
        logger.info('failed to convert to dataframe returns empty dataframe', e)
        df1 = pd.DataFrame()

    return df1, list(warnings), list(errors)



